.. |release| replace:: CURRENT_MINOR
.. |current-major-version| replace:: 3006
.. |supported-release-1| replace:: LATEST_MINOR
.. |supported-release-2| replace:: OLD_MINOR
.. |supported-release-3| replace:: OLDEST_MINOR
.. |supported-release-1-badge| replace:: :bdg-link-success:`LATEST_MINOR <https://docs.saltproject.io/en/latest/topics/releases/LATEST_MINOR.html>`
.. |supported-release-2-badge| replace:: :bdg-link-primary:`OLD_MINOR <https://docs.saltproject.io/en/OLD_MAJOR/topics/releases/OLD_MINOR.html>`
.. |supported-release-3-badge| replace:: :bdg-link-warning:`OLDEST_MINOR <https://docs.saltproject.io/en/OLDEST_MAJOR/topics/releases/OLDEST_MINOR.html>`

.. |quickstart-script-path| replace:: https://raw.githubusercontent.com/saltstack/salt-bootstrap/develop/salt-quick-start.sh
.. |quickstart-script-path-windows| replace:: https://raw.githubusercontent.com/saltstack/salt-bootstrap/develop/salt-quick-start.ps1

.. |release-candidate-version| replace:: RC_RELEASE
.. |debian-release-candidate-gpg| replace:: /etc/apt/keyrings/salt-archive-keyring-2023.gpg https://repo.saltproject.io/salt_rc/salt/py3/debian/11/amd64/latest/SALT-PROJECT-GPG-PUBKEY-2023.gpg
.. |debian-release-candidate| replace:: [signed-by=/etc/apt/keyrings/salt-archive-keyring-2023.gpg] https://repo.saltproject.io/salt_rc/salt/py3/debian/11/amd64/latest/ bullseye main"
.. |rhel-release-candidate-gpg| replace:: https://repo.saltproject.io/salt_rc/salt/py3/redhat/9/x86_64/latest/SALT-PROJECT-GPG-PUBKEY-2023.pub
.. |rhel-release-candidate| replace:: https://repo.saltproject.io/salt_rc/salt/py3/redhat/9/x86_64/latest.repo
.. |rhel-release-candidate-echo| replace:: 'baseurl=https://repo.saltproject.io/salt_rc/salt/py3/redhat/$releasever/$basearch/latest'
.. |ubuntu-release-candidate-gpg| replace:: /etc/apt/keyrings/salt-archive-keyring-2023.gpg https://repo.saltproject.io/salt_rc/salt/py3/ubuntu/22.04/amd64/latest/SALT-PROJECT-GPG-PUBKEY-2023.gpg
.. |ubuntu-release-candidate| replace:: [signed-by=/etc/apt/keyrings/salt-archive-keyring-2023.gpg] https://repo.saltproject.io/salt_rc/salt/py3/ubuntu/22.04/amd64/latest/ jammy main"
.. |bootstrap-release-candidate| replace:: python3 git vRC_RELEASE
.. |pip-install-release-candidate| replace:: sudo pip install salt==RC_RELEASE

.. |amazon-linux2-latest-gpg-x86_64| replace:: https://repo.saltproject.io/salt/py3/amazon/2/x86_64/SALT-PROJECT-GPG-PUBKEY-2023.pub
.. |amazon-linux2-latest-download-x86_64| replace:: https://repo.saltproject.io/salt/py3/amazon/2/x86_64/latest.repo
.. |amazon-linux2-major-gpg-x86_64| replace:: https://repo.saltproject.io/salt/py3/amazon/2/x86_64/SALT-PROJECT-GPG-PUBKEY-2023.pub
.. |amazon-linux2-major-download-x86_64| replace:: https://repo.saltproject.io/salt/py3/amazon/2/x86_64/CURRENT_MAJOR.repo
.. |amazon-linux2-minor-gpg-x86_64| replace:: https://repo.saltproject.io/salt/py3/amazon/2/x86_64/SALT-PROJECT-GPG-PUBKEY-2023.pub
.. |amazon-linux2-minor-download-x86_64| replace:: https://repo.saltproject.io/salt/py3/amazon/2/x86_64/minor/CURRENT_MINOR.repo

.. |amazon-linux2-latest-gpg-arm64| replace:: https://repo.saltproject.io/salt/py3/amazon/2/arm64/SALT-PROJECT-GPG-PUBKEY-2023.pub
.. |amazon-linux2-latest-download-arm64| replace:: https://repo.saltproject.io/salt/py3/amazon/2/arm64/latest.repo
.. |amazon-linux2-major-gpg-arm64| replace:: https://repo.saltproject.io/salt/py3/amazon/2/arm64/SALT-PROJECT-GPG-PUBKEY-2023.pub
.. |amazon-linux2-major-download-arm64| replace:: https://repo.saltproject.io/salt/py3/amazon/2/arm64/CURRENT_MAJOR.repo
.. |amazon-linux2-minor-gpg-arm64| replace:: https://repo.saltproject.io/salt/py3/amazon/2/arm64/SALT-PROJECT-GPG-PUBKEY-2023.pub
.. |amazon-linux2-minor-download-arm64| replace:: https://repo.saltproject.io/salt/py3/amazon/2/arm64/minor/CURRENT_MINOR.repo

.. |amazon-linux2023-latest-gpg-x86_64| replace:: https://repo.saltproject.io/salt/py3/amazon/2023/x86_64/SALT-PROJECT-GPG-PUBKEY-2023.pub
.. |amazon-linux2023-latest-download-x86_64| replace:: https://repo.saltproject.io/salt/py3/amazon/2023/x86_64/latest.repo
.. |amazon-linux2023-major-gpg-x86_64| replace:: https://repo.saltproject.io/salt/py3/amazon/2023/x86_64/SALT-PROJECT-GPG-PUBKEY-2023.pub
.. |amazon-linux2023-major-download-x86_64| replace:: https://repo.saltproject.io/salt/py3/amazon/2023/x86_64/CURRENT_MAJOR.repo
.. |amazon-linux2023-minor-gpg-x86_64| replace:: https://repo.saltproject.io/salt/py3/amazon/2023/x86_64/SALT-PROJECT-GPG-PUBKEY-2023.pub
.. |amazon-linux2023-minor-download-x86_64| replace:: https://repo.saltproject.io/salt/py3/amazon/2023/x86_64/minor/CURRENT_MINOR.repo

.. |amazon-linux2023-latest-gpg-arm64| replace:: https://repo.saltproject.io/salt/py3/amazon/2023/arm64/SALT-PROJECT-GPG-PUBKEY-2023.pub
.. |amazon-linux2023-latest-download-arm64| replace:: https://repo.saltproject.io/salt/py3/amazon/2023/arm64/latest.repo
.. |amazon-linux2023-major-gpg-arm64| replace:: https://repo.saltproject.io/salt/py3/amazon/2023/arm64/SALT-PROJECT-GPG-PUBKEY-2023.pub
.. |amazon-linux2023-major-download-arm64| replace:: https://repo.saltproject.io/salt/py3/amazon/2023/arm64/CURRENT_MAJOR.repo
.. |amazon-linux2023-minor-gpg-arm64| replace:: https://repo.saltproject.io/salt/py3/amazon/2023/arm64/SALT-PROJECT-GPG-PUBKEY-2023.pub
.. |amazon-linux2023-minor-download-arm64| replace:: https://repo.saltproject.io/salt/py3/amazon/2023/arm64/minor/CURRENT_MINOR.repo

.. |centos9-latest-gpg-x86_64| replace:: https://repo.saltproject.io/salt/py3/redhat/9/x86_64/SALT-PROJECT-GPG-PUBKEY-2023.pub
.. |centos9-latest-download-x86_64| replace:: https://repo.saltproject.io/salt/py3/redhat/9/x86_64/latest.repo
.. |centos9-major-gpg-x86_64| replace:: https://repo.saltproject.io/salt/py3/redhat/9/x86_64/SALT-PROJECT-GPG-PUBKEY-2023.pub
.. |centos9-major-download-x86_64| replace:: https://repo.saltproject.io/salt/py3/redhat/9/x86_64/CURRENT_MAJOR.repo
.. |centos9-minor-gpg-x86_64| replace:: https://repo.saltproject.io/salt/py3/redhat/9/x86_64/SALT-PROJECT-GPG-PUBKEY-2023.pub
.. |centos9-minor-download-x86_64| replace:: https://repo.saltproject.io/salt/py3/redhat/9/x86_64/minor/CURRENT_MINOR.repo

.. |centos9-latest-gpg-arm64| replace:: https://repo.saltproject.io/salt/py3/redhat/9/arm64/SALT-PROJECT-GPG-PUBKEY-2023.pub
.. |centos9-latest-download-arm64| replace:: https://repo.saltproject.io/salt/py3/redhat/9/arm64/latest.repo
.. |centos9-major-gpg-arm64| replace:: https://repo.saltproject.io/salt/py3/redhat/9/arm64/SALT-PROJECT-GPG-PUBKEY-2023.pub
.. |centos9-major-download-arm64| replace:: https://repo.saltproject.io/salt/py3/redhat/9/arm64/CURRENT_MAJOR.repo
.. |centos9-minor-gpg-arm64| replace:: https://repo.saltproject.io/salt/py3/redhat/9/arm64/SALT-PROJECT-GPG-PUBKEY-2023.pub
.. |centos9-minor-download-arm64| replace:: https://repo.saltproject.io/salt/py3/redhat/9/arm64/minor/CURRENT_MINOR.repo

.. |centos8-latest-gpg-x86_64| replace:: https://repo.saltproject.io/salt/py3/redhat/8/x86_64/SALT-PROJECT-GPG-PUBKEY-2023.pub
.. |centos8-latest-download-x86_64| replace:: https://repo.saltproject.io/salt/py3/redhat/8/x86_64/latest.repo
.. |centos8-major-gpg-x86_64| replace:: https://repo.saltproject.io/salt/py3/redhat/8/x86_64/SALT-PROJECT-GPG-PUBKEY-2023.pub
.. |centos8-major-download-x86_64| replace:: https://repo.saltproject.io/salt/py3/redhat/8/x86_64/CURRENT_MAJOR.repo
.. |centos8-minor-gpg-x86_64| replace:: https://repo.saltproject.io/salt/py3/redhat/8/x86_64/SALT-PROJECT-GPG-PUBKEY-2023.pub
.. |centos8-minor-download-x86_64| replace:: https://repo.saltproject.io/salt/py3/redhat/8/x86_64/minor/CURRENT_MINOR.repo

.. |centos8-latest-gpg-arm64| replace:: https://repo.saltproject.io/salt/py3/redhat/8/arm64/SALT-PROJECT-GPG-PUBKEY-2023.pub
.. |centos8-latest-download-arm64| replace:: https://repo.saltproject.io/salt/py3/redhat/8/arm64/latest.repo
.. |centos8-major-gpg-arm64| replace:: https://repo.saltproject.io/salt/py3/redhat/8/arm64/SALT-PROJECT-GPG-PUBKEY-2023.pub
.. |centos8-major-download-arm64| replace:: https://repo.saltproject.io/salt/py3/redhat/8/arm64/CURRENT_MAJOR.repo
.. |centos8-minor-gpg-arm64| replace:: https://repo.saltproject.io/salt/py3/redhat/8/arm64/SALT-PROJECT-GPG-PUBKEY-2023.pub
.. |centos8-minor-download-arm64| replace:: https://repo.saltproject.io/salt/py3/redhat/8/arm64/minor/CURRENT_MINOR.repo

.. |centos7-latest-gpg-x86_64| replace:: https://repo.saltproject.io/salt/py3/redhat/7/x86_64/SALT-PROJECT-GPG-PUBKEY-2023.pub
.. |centos7-latest-download-x86_64| replace:: https://repo.saltproject.io/salt/py3/redhat/7/x86_64/latest.repo
.. |centos7-major-gpg-x86_64| replace:: https://repo.saltproject.io/salt/py3/redhat/7/x86_64/SALT-PROJECT-GPG-PUBKEY-2023.pub
.. |centos7-major-download-x86_64| replace:: https://repo.saltproject.io/salt/py3/redhat/7/x86_64/CURRENT_MAJOR.repo
.. |centos7-minor-gpg-x86_64| replace:: https://repo.saltproject.io/salt/py3/redhat/7/x86_64/SALT-PROJECT-GPG-PUBKEY-2023.pub
.. |centos7-minor-download-x86_64| replace:: https://repo.saltproject.io/salt/py3/redhat/7/x86_64/minor/CURRENT_MINOR.repo

.. |centos7-latest-gpg-arm64| replace:: https://repo.saltproject.io/salt/py3/redhat/7/arm64/SALT-PROJECT-GPG-PUBKEY-2023.pub
.. |centos7-latest-download-arm64| replace:: https://repo.saltproject.io/salt/py3/redhat/7/arm64/latest.repo
.. |centos7-major-gpg-arm64| replace:: https://repo.saltproject.io/salt/py3/redhat/7/arm64/SALT-PROJECT-GPG-PUBKEY-2023.pub
.. |centos7-major-download-arm64| replace:: https://repo.saltproject.io/salt/py3/redhat/7/arm64/CURRENT_MAJOR.repo
.. |centos7-minor-gpg-arm64| replace:: https://repo.saltproject.io/salt/py3/redhat/7/arm64/SALT-PROJECT-GPG-PUBKEY-2023.pub
.. |centos7-minor-download-arm64| replace:: https://repo.saltproject.io/salt/py3/redhat/7/arm64/minor/CURRENT_MINOR.repo

.. |debian12-latest-gpg-amd64| replace:: https://repo.saltproject.io/salt/py3/debian/12/amd64/SALT-PROJECT-GPG-PUBKEY-2023.gpg
.. |debian12-latest-download-amd64| replace:: https://repo.saltproject.io/salt/py3/debian/12/amd64/latest bookworm main
.. |debian12-major-gpg-amd64| replace:: https://repo.saltproject.io/salt/py3/debian/12/amd64/SALT-PROJECT-GPG-PUBKEY-2023.gpg
.. |debian12-major-download-amd64| replace:: https://repo.saltproject.io/salt/py3/debian/12/amd64/CURRENT_MAJOR bookworm main
.. |debian12-minor-gpg-amd64| replace:: https://repo.saltproject.io/salt/py3/debian/12/amd64/SALT-PROJECT-GPG-PUBKEY-2023.gpg
.. |debian12-minor-download-amd64| replace:: https://repo.saltproject.io/salt/py3/debian/12/amd64/minor/CURRENT_MINOR bookworm main

.. |debian12-latest-gpg-arm64| replace:: https://repo.saltproject.io/salt/py3/debian/12/arm64/SALT-PROJECT-GPG-PUBKEY-2023.gpg
.. |debian12-latest-download-arm64| replace:: https://repo.saltproject.io/salt/py3/debian/12/arm64/latest bookworm main
.. |debian12-major-gpg-arm64| replace:: https://repo.saltproject.io/salt/py3/debian/12/arm64/SALT-PROJECT-GPG-PUBKEY-2023.gpg
.. |debian12-major-download-arm64| replace:: https://repo.saltproject.io/salt/py3/debian/12/arm64/CURRENT_MAJOR bookworm main
.. |debian12-minor-gpg-arm64| replace:: https://repo.saltproject.io/salt/py3/debian/12/arm64/SALT-PROJECT-GPG-PUBKEY-2023.gpg
.. |debian12-minor-download-arm64| replace:: https://repo.saltproject.io/salt/py3/debian/12/arm64/minor/CURRENT_MINOR bookworm main

.. |debian11-latest-gpg-amd64| replace:: https://repo.saltproject.io/salt/py3/debian/11/amd64/SALT-PROJECT-GPG-PUBKEY-2023.gpg
.. |debian11-latest-download-amd64| replace:: https://repo.saltproject.io/salt/py3/debian/11/amd64/latest bullseye main
.. |debian11-major-gpg-amd64| replace:: https://repo.saltproject.io/salt/py3/debian/11/amd64/SALT-PROJECT-GPG-PUBKEY-2023.gpg
.. |debian11-major-download-amd64| replace:: https://repo.saltproject.io/salt/py3/debian/11/amd64/CURRENT_MAJOR bullseye main
.. |debian11-minor-gpg-amd64| replace:: https://repo.saltproject.io/salt/py3/debian/11/amd64/SALT-PROJECT-GPG-PUBKEY-2023.gpg
.. |debian11-minor-download-amd64| replace:: https://repo.saltproject.io/salt/py3/debian/11/amd64/minor/CURRENT_MINOR bullseye main

.. |debian11-latest-gpg-arm64| replace:: https://repo.saltproject.io/salt/py3/debian/11/arm64/SALT-PROJECT-GPG-PUBKEY-2023.gpg
.. |debian11-latest-download-arm64| replace:: https://repo.saltproject.io/salt/py3/debian/11/arm64/latest bullseye main
.. |debian11-major-gpg-arm64| replace:: https://repo.saltproject.io/salt/py3/debian/11/arm64/SALT-PROJECT-GPG-PUBKEY-2023.gpg
.. |debian11-major-download-arm64| replace:: https://repo.saltproject.io/salt/py3/debian/11/arm64/CURRENT_MAJOR bullseye main
.. |debian11-minor-gpg-arm64| replace:: https://repo.saltproject.io/salt/py3/debian/11/arm64/SALT-PROJECT-GPG-PUBKEY-2023.gpg
.. |debian11-minor-download-arm64| replace:: https://repo.saltproject.io/salt/py3/debian/11/arm64/minor/CURRENT_MINOR bullseye main

.. |debian10-latest-gpg-amd64| replace:: https://repo.saltproject.io/salt/py3/debian/10/amd64/SALT-PROJECT-GPG-PUBKEY-2023.gpg
.. |debian10-latest-download-amd64| replace:: https://repo.saltproject.io/salt/py3/debian/10/amd64/latest buster main
.. |debian10-major-gpg-amd64| replace:: https://repo.saltproject.io/salt/py3/debian/10/amd64/SALT-PROJECT-GPG-PUBKEY-2023.gpg
.. |debian10-major-download-amd64| replace:: https://repo.saltproject.io/salt/py3/debian/10/amd64/CURRENT_MAJOR buster main
.. |debian10-minor-gpg-amd64| replace:: https://repo.saltproject.io/salt/py3/debian/10/amd64/SALT-PROJECT-GPG-PUBKEY-2023.gpg
.. |debian10-minor-download-amd64| replace:: https://repo.saltproject.io/salt/py3/debian/10/amd64/minor/CURRENT_MINOR buster main

.. |debian10-latest-gpg-arm64| replace:: https://repo.saltproject.io/salt/py3/debian/10/arm64/SALT-PROJECT-GPG-PUBKEY-2023.gpg
.. |debian10-latest-download-arm64| replace:: https://repo.saltproject.io/salt/py3/debian/10/arm64/latest buster main
.. |debian10-major-gpg-arm64| replace:: https://repo.saltproject.io/salt/py3/debian/10/arm64/SALT-PROJECT-GPG-PUBKEY-2023.gpg
.. |debian10-major-download-arm64| replace:: https://repo.saltproject.io/salt/py3/debian/10/arm64/CURRENT_MAJOR buster main
.. |debian10-minor-gpg-arm64| replace:: https://repo.saltproject.io/salt/py3/debian/10/arm64/SALT-PROJECT-GPG-PUBKEY-2023.gpg
.. |debian10-minor-download-arm64| replace:: https://repo.saltproject.io/salt/py3/debian/10/arm64/minor/CURRENT_MINOR buster main

.. |fedora38-latest-gpg-x86_64| replace:: https://repo.saltproject.io/salt/py3/fedora/38/x86_64/SALT-PROJECT-GPG-PUBKEY-2023.pub
.. |fedora38-latest-download-x86_64| replace:: https://repo.saltproject.io/salt/py3/fedora/38/x86_64/latest.repo
.. |fedora38-major-gpg-x86_64| replace:: https://repo.saltproject.io/salt/py3/fedora/38/x86_64/SALT-PROJECT-GPG-PUBKEY-2023.pub
.. |fedora38-major-download-x86_64| replace:: https://repo.saltproject.io/salt/py3/fedora/38/x86_64/CURRENT_MAJOR.repo
.. |fedora38-minor-gpg-x86_64| replace:: https://repo.saltproject.io/salt/py3/fedora/38/x86_64/SALT-PROJECT-GPG-PUBKEY-2023.pub
.. |fedora38-minor-download-x86_64| replace:: https://repo.saltproject.io/salt/py3/fedora/38/x86_64/minor/CURRENT_MINOR.repo

.. |fedora38-latest-gpg-arm64| replace:: https://repo.saltproject.io/salt/py3/fedora/38/arm64/SALT-PROJECT-GPG-PUBKEY-2023.pub
.. |fedora38-latest-download-arm64| replace:: https://repo.saltproject.io/salt/py3/fedora/38/arm64/latest.repo
.. |fedora38-major-gpg-arm64| replace:: https://repo.saltproject.io/salt/py3/fedora/38/arm64/SALT-PROJECT-GPG-PUBKEY-2023.pub
.. |fedora38-major-download-arm64| replace:: https://repo.saltproject.io/salt/py3/fedora/38/arm64/CURRENT_MAJOR.repo
.. |fedora38-minor-gpg-arm64| replace:: https://repo.saltproject.io/salt/py3/fedora/38/arm64/SALT-PROJECT-GPG-PUBKEY-2023.pub
.. |fedora38-minor-download-arm64| replace:: https://repo.saltproject.io/salt/py3/fedora/38/arm64/minor/CURRENT_MINOR.repo

.. |fedora37-latest-gpg-x86_64| replace:: https://repo.saltproject.io/salt/py3/fedora/37/x86_64/SALT-PROJECT-GPG-PUBKEY-2023.pub
.. |fedora37-latest-download-x86_64| replace:: https://repo.saltproject.io/salt/py3/fedora/37/x86_64/latest.repo
.. |fedora37-major-gpg-x86_64| replace:: https://repo.saltproject.io/salt/py3/fedora/37/x86_64/SALT-PROJECT-GPG-PUBKEY-2023.pub
.. |fedora37-major-download-x86_64| replace:: https://repo.saltproject.io/salt/py3/fedora/37/x86_64/CURRENT_MAJOR.repo
.. |fedora37-minor-gpg-x86_64| replace:: https://repo.saltproject.io/salt/py3/fedora/37/x86_64/SALT-PROJECT-GPG-PUBKEY-2023.pub
.. |fedora37-minor-download-x86_64| replace:: https://repo.saltproject.io/salt/py3/fedora/37/x86_64/minor/CURRENT_MINOR.repo

.. |fedora37-latest-gpg-arm64| replace:: https://repo.saltproject.io/salt/py3/fedora/37/arm64/SALT-PROJECT-GPG-PUBKEY-2023.pub
.. |fedora37-latest-download-arm64| replace:: https://repo.saltproject.io/salt/py3/fedora/37/arm64/latest.repo
.. |fedora37-major-gpg-arm64| replace:: https://repo.saltproject.io/salt/py3/fedora/37/arm64/SALT-PROJECT-GPG-PUBKEY-2023.pub
.. |fedora37-major-download-arm64| replace:: https://repo.saltproject.io/salt/py3/fedora/37/arm64/CURRENT_MAJOR.repo
.. |fedora37-minor-gpg-arm64| replace:: https://repo.saltproject.io/salt/py3/fedora/37/arm64/SALT-PROJECT-GPG-PUBKEY-2023.pub
.. |fedora37-minor-download-arm64| replace:: https://repo.saltproject.io/salt/py3/fedora/37/arm64/minor/CURRENT_MINOR.repo

.. |macos-amd64-download| replace:: https://repo.saltproject.io/salt/py3/macos/latest/salt-CURRENT_MINOR-py3-x86_64.pkg
.. |macos-amd64-gpg| replace:: https://repo.saltproject.io/salt/py3/macos/SALT-PROJECT-GPG-PUBKEY-2023.gpg
.. |macos-arm64-download| replace:: https://repo.saltproject.io/salt/py3/macos/latest/salt-CURRENT_MINOR-py3-arm64.pkg

.. |photonos5-latest-gpg| replace:: https://repo.saltproject.io/salt/py3/photon/5.0/x86_64/latest/SALT-PROJECT-GPG-PUBKEY-2023.pub
.. |photonos5-latest-download| replace:: https://repo.saltproject.io/salt/py3/photon/5.0/x86_64/latest.repo
.. |photonos5-major-gpg| replace:: https://repo.saltproject.io/salt/py3/photon/5.0/x86_64/latest/SALT-PROJECT-GPG-PUBKEY-2023.pub
.. |photonos5-major-download| replace:: https://repo.saltproject.io/salt/py3/photon/5.0/x86_64/CURRENT_MAJOR.repo
.. |photonos5-minor-gpg| replace:: https://repo.saltproject.io/salt/py3/photon/5.0/x86_64/SALT-PROJECT-GPG-PUBKEY-2023.pub
.. |photonos5-minor-download| replace:: https://repo.saltproject.io/salt/py3/photon/5.0/x86_64/minor/CURRENT_MINOR.repo

.. |photonos4-latest-gpg| replace:: https://repo.saltproject.io/salt/py3/photon/4.0/x86_64/latest/SALT-PROJECT-GPG-PUBKEY-2023.pub
.. |photonos4-latest-download| replace:: https://repo.saltproject.io/salt/py3/photon/4.0/x86_64/latest.repo
.. |photonos4-major-gpg| replace:: https://repo.saltproject.io/salt/py3/photon/4.0/x86_64/latest/SALT-PROJECT-GPG-PUBKEY-2023.pub
.. |photonos4-major-download| replace:: https://repo.saltproject.io/salt/py3/photon/4.0/x86_64/CURRENT_MAJOR.repo
.. |photonos4-minor-gpg| replace:: https://repo.saltproject.io/salt/py3/photon/4.0/x86_64/SALT-PROJECT-GPG-PUBKEY-2023.pub
.. |photonos4-minor-download| replace:: https://repo.saltproject.io/salt/py3/photon/4.0/x86_64/minor/CURRENT_MINOR.repo

.. |photonos3-latest-gpg| replace:: https://repo.saltproject.io/salt/py3/photon/3.0/x86_64/latest/SALT-PROJECT-GPG-PUBKEY-2023.pub
.. |photonos3-latest-download| replace:: https://repo.saltproject.io/salt/py3/photon/3.0/x86_64/latest.repo
.. |photonos3-major-gpg| replace:: https://repo.saltproject.io/salt/py3/photon/3.0/x86_64/latest/SALT-PROJECT-GPG-PUBKEY-2023.pub
.. |photonos3-major-download| replace:: https://repo.saltproject.io/salt/py3/photon/3.0/x86_64/CURRENT_MAJOR.repo
.. |photonos3-minor-gpg| replace:: https://repo.saltproject.io/salt/py3/photon/3.0/x86_64/SALT-PROJECT-GPG-PUBKEY-2023.pub
.. |photonos3-minor-download| replace:: https://repo.saltproject.io/salt/py3/photon/3.0/x86_64/minor/CURRENT_MINOR.repo

.. |rhel9-latest-gpg-x86_64| replace:: https://repo.saltproject.io/salt/py3/redhat/9/x86_64/SALT-PROJECT-GPG-PUBKEY-2023.pub
.. |rhel9-latest-download-x86_64| replace:: https://repo.saltproject.io/salt/py3/redhat/9/x86_64/latest.repo
.. |rhel9-major-gpg-x86_64| replace:: https://repo.saltproject.io/salt/py3/redhat/9/x86_64/SALT-PROJECT-GPG-PUBKEY-2023.pub
.. |rhel9-major-download-x86_64| replace:: https://repo.saltproject.io/salt/py3/redhat/9/x86_64/CURRENT_MAJOR.repo
.. |rhel9-minor-gpg-x86_64| replace:: https://repo.saltproject.io/salt/py3/redhat/9/x86_64/SALT-PROJECT-GPG-PUBKEY-2023.pub
.. |rhel9-minor-download-x86_64| replace:: https://repo.saltproject.io/salt/py3/redhat/9/x86_64/minor/CURRENT_MINOR.repo

.. |rhel9-latest-gpg-arm64| replace:: https://repo.saltproject.io/salt/py3/redhat/9/arm64/SALT-PROJECT-GPG-PUBKEY-2023.pub
.. |rhel9-latest-download-arm64| replace:: https://repo.saltproject.io/salt/py3/redhat/9/arm64/latest.repo
.. |rhel9-major-gpg-arm64| replace:: https://repo.saltproject.io/salt/py3/redhat/9/arm64/SALT-PROJECT-GPG-PUBKEY-2023.pub
.. |rhel9-major-download-arm64| replace:: https://repo.saltproject.io/salt/py3/redhat/9/arm64/CURRENT_MAJOR.repo
.. |rhel9-minor-gpg-arm64| replace:: https://repo.saltproject.io/salt/py3/redhat/9/arm64/SALT-PROJECT-GPG-PUBKEY-2023.pub
.. |rhel9-minor-download-arm64| replace:: https://repo.saltproject.io/salt/py3/redhat/9/arm64/minor/CURRENT_MINOR.repo

.. |rhel8-latest-gpg-x86_64| replace:: https://repo.saltproject.io/salt/py3/redhat/8/x86_64/SALT-PROJECT-GPG-PUBKEY-2023.pub
.. |rhel8-latest-download-x86_64| replace:: https://repo.saltproject.io/salt/py3/redhat/8/x86_64/latest.repo
.. |rhel8-major-gpg-x86_64| replace:: https://repo.saltproject.io/salt/py3/redhat/8/x86_64/SALT-PROJECT-GPG-PUBKEY-2023.pub
.. |rhel8-major-download-x86_64| replace:: https://repo.saltproject.io/salt/py3/redhat/8/x86_64/CURRENT_MAJOR.repo
.. |rhel8-minor-gpg-x86_64| replace:: https://repo.saltproject.io/salt/py3/redhat/8/x86_64/SALT-PROJECT-GPG-PUBKEY-2023.pub
.. |rhel8-minor-download-x86_64| replace:: https://repo.saltproject.io/salt/py3/redhat/8/x86_64/minor/CURRENT_MINOR.repo

.. |rhel8-latest-gpg-arm64| replace:: https://repo.saltproject.io/salt/py3/redhat/8/arm64/SALT-PROJECT-GPG-PUBKEY-2023.pub
.. |rhel8-latest-download-arm64| replace:: https://repo.saltproject.io/salt/py3/redhat/8/arm64/latest.repo
.. |rhel8-major-gpg-arm64| replace:: https://repo.saltproject.io/salt/py3/redhat/8/arm64/SALT-PROJECT-GPG-PUBKEY-2023.pub
.. |rhel8-major-download-arm64| replace:: https://repo.saltproject.io/salt/py3/redhat/8/arm64/CURRENT_MAJOR.repo
.. |rhel8-minor-gpg-arm64| replace:: https://repo.saltproject.io/salt/py3/redhat/8/arm64/SALT-PROJECT-GPG-PUBKEY-2023.pub
.. |rhel8-minor-download-arm64| replace:: https://repo.saltproject.io/salt/py3/redhat/8/arm64/minor/CURRENT_MINOR.repo

.. |rhel7-latest-gpg-x86_64| replace:: https://repo.saltproject.io/salt/py3/redhat/7/x86_64/SALT-PROJECT-GPG-PUBKEY-2023.pub
.. |rhel7-latest-download-x86_64| replace:: https://repo.saltproject.io/salt/py3/redhat/7/x86_64/latest.repo
.. |rhel7-major-gpg-x86_64| replace:: https://repo.saltproject.io/salt/py3/redhat/7/x86_64/SALT-PROJECT-GPG-PUBKEY-2023.pub
.. |rhel7-major-download-x86_64| replace:: https://repo.saltproject.io/salt/py3/redhat/7/x86_64/CURRENT_MAJOR.repo
.. |rhel7-minor-gpg-x86_64| replace:: https://repo.saltproject.io/salt/py3/redhat/7/x86_64/SALT-PROJECT-GPG-PUBKEY-2023.pub
.. |rhel7-minor-download-x86_64| replace:: https://repo.saltproject.io/salt/py3/redhat/7/x86_64/minor/CURRENT_MINOR.repo

.. |rhel7-latest-gpg-arm64| replace:: https://repo.saltproject.io/salt/py3/redhat/7/arm64/SALT-PROJECT-GPG-PUBKEY-2023.pub
.. |rhel7-latest-download-arm64| replace:: https://repo.saltproject.io/salt/py3/redhat/7/arm64/latest.repo
.. |rhel7-major-gpg-arm64| replace:: https://repo.saltproject.io/salt/py3/redhat/7/arm64/SALT-PROJECT-GPG-PUBKEY-2023.pub
.. |rhel7-major-download-arm64| replace:: https://repo.saltproject.io/salt/py3/redhat/7/arm64/CURRENT_MAJOR.repo
.. |rhel7-minor-gpg-arm64| replace:: https://repo.saltproject.io/salt/py3/redhat/7/arm64/SALT-PROJECT-GPG-PUBKEY-2023.pub
.. |rhel7-minor-download-arm64| replace:: https://repo.saltproject.io/salt/py3/redhat/7/arm64/minor/CURRENT_MINOR.repo

.. |ubuntu22-latest-gpg-amd64| replace:: https://repo.saltproject.io/salt/py3/ubuntu/22.04/amd64/SALT-PROJECT-GPG-PUBKEY-2023.gpg
.. |ubuntu22-latest-download-amd64| replace:: https://repo.saltproject.io/salt/py3/ubuntu/22.04/amd64/latest jammy main
.. |ubuntu22-major-gpg-amd64| replace:: https://repo.saltproject.io/salt/py3/ubuntu/22.04/amd64/SALT-PROJECT-GPG-PUBKEY-2023.gpg
.. |ubuntu22-major-download-amd64| replace:: https://repo.saltproject.io/salt/py3/ubuntu/22.04/amd64/CURRENT_MAJOR jammy main
.. |ubuntu22-minor-gpg-amd64| replace:: https://repo.saltproject.io/salt/py3/ubuntu/22.04/amd64/SALT-PROJECT-GPG-PUBKEY-2023.gpg
.. |ubuntu22-minor-download-amd64| replace:: https://repo.saltproject.io/salt/py3/ubuntu/22.04/amd64/minor/CURRENT_MINOR jammy main

.. |ubuntu22-latest-gpg-arm64| replace:: https://repo.saltproject.io/salt/py3/ubuntu/22.04/arm64/SALT-PROJECT-GPG-PUBKEY-2023.gpg
.. |ubuntu22-latest-download-arm64| replace:: https://repo.saltproject.io/salt/py3/ubuntu/22.04/arm64/latest jammy main
.. |ubuntu22-major-gpg-arm64| replace:: https://repo.saltproject.io/salt/py3/ubuntu/22.04/arm64/SALT-PROJECT-GPG-PUBKEY-2023.gpg
.. |ubuntu22-major-download-arm64| replace:: https://repo.saltproject.io/salt/py3/ubuntu/22.04/arm64/CURRENT_MAJOR jammy main
.. |ubuntu22-minor-gpg-arm64| replace:: https://repo.saltproject.io/salt/py3/ubuntu/22.04/arm64/SALT-PROJECT-GPG-PUBKEY-2023.gpg
.. |ubuntu22-minor-download-arm64| replace:: https://repo.saltproject.io/salt/py3/ubuntu/22.04/arm64/minor/CURRENT_MINOR jammy main

.. |ubuntu20-latest-gpg-amd64| replace:: https://repo.saltproject.io/salt/py3/ubuntu/20.04/amd64/SALT-PROJECT-GPG-PUBKEY-2023.gpg
.. |ubuntu20-latest-download-amd64| replace:: https://repo.saltproject.io/salt/py3/ubuntu/20.04/amd64/latest focal main
.. |ubuntu20-major-gpg-amd64| replace:: https://repo.saltproject.io/salt/py3/ubuntu/20.04/amd64/SALT-PROJECT-GPG-PUBKEY-2023.gpg
.. |ubuntu20-major-download-amd64| replace:: https://repo.saltproject.io/salt/py3/ubuntu/20.04/amd64/CURRENT_MAJOR focal main
.. |ubuntu20-minor-gpg-amd64| replace:: https://repo.saltproject.io/salt/py3/ubuntu/20.04/amd64/SALT-PROJECT-GPG-PUBKEY-2023.gpg
.. |ubuntu20-minor-download-amd64| replace:: https://repo.saltproject.io/salt/py3/ubuntu/20.04/amd64/minor/CURRENT_MINOR focal main

.. |ubuntu20-latest-gpg-arm64| replace:: https://repo.saltproject.io/salt/py3/ubuntu/20.04/arm64/SALT-PROJECT-GPG-PUBKEY-2023.gpg
.. |ubuntu20-latest-download-arm64| replace:: https://repo.saltproject.io/salt/py3/ubuntu/20.04/arm64/latest focal main
.. |ubuntu20-major-gpg-arm64| replace:: https://repo.saltproject.io/salt/py3/ubuntu/20.04/arm64/SALT-PROJECT-GPG-PUBKEY-2023.gpg
.. |ubuntu20-major-download-arm64| replace:: https://repo.saltproject.io/salt/py3/ubuntu/20.04/arm64/CURRENT_MAJOR focal main
.. |ubuntu20-minor-gpg-arm64| replace:: https://repo.saltproject.io/salt/py3/ubuntu/20.04/arm64/SALT-PROJECT-GPG-PUBKEY-2023.gpg
.. |ubuntu20-minor-download-arm64| replace:: https://repo.saltproject.io/salt/py3/ubuntu/20.04/arm64/minor/CURRENT_MINOR focal main

.. |windows-install-exe-example| replace:: Salt-Minion-CURRENT_MINOR-Py3-AMD64-Setup.exe
.. |windows-install-msi-example| replace:: Salt-Minion-CURRENT_MINOR-Py3-AMD64.msi

.. |windows-amd64-exe-gpg| replace:: https://repo.saltproject.io/salt/py3/windows/SALT-PROJECT-GPG-PUBKEY-2023.gpg
.. |windows-amd64-exe-download| replace:: https://repo.saltproject.io/salt/py3/windows/latest/Salt-Minion-CURRENT_MINOR-Py3-AMD64-Setup.exe
