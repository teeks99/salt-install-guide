.. Warning::

   The 3006 version of Salt for Photon OS, starting with 3006.5, is FIPS compliant.

   If your organization requires FIPS compliance and you need to install Salt on
   Photon OS 3 or 4, install at least the 3006.5 version of Salt.
